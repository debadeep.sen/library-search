import { getFullEndPoint } from '@/utils/utilities'
import { describe, test, expect } from 'vitest'

describe('Testing utilities', () => {
  test('full API URL, default', () => {
    expect(getFullEndPoint('test')).toEqual(
      'https://openlibrary.org/search.json?q=test'
    )
  })
  test('full API URL, title', () => {
    expect(getFullEndPoint('test', 'title')).toEqual(
      'https://openlibrary.org/search.json?title=test'
    )
  })
})
