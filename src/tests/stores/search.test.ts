/* eslint-disable @typescript-eslint/ban-types */
import { describe, test, expect, beforeEach } from 'vitest'
import { createPinia, setActivePinia, type Store } from 'pinia'
import { useSearchStore } from '@/stores/search'
import type { Book } from '@/types/book'

describe('Testing loader store', () => {
  let searchStore: Store<
    'search',
    {
      searchTerm: string
      numFound: number | null
      searchResults: Book[]
      currentBook: Book
    },
    {},
    {}
  >
  beforeEach(() => {
    const pinia = createPinia()
    setActivePinia(pinia)
    searchStore = useSearchStore()
  })

  test('initial values', () => {
    expect(searchStore.searchTerm).toEqual('')
    expect(searchStore.numFound).toBeNull()
    expect(searchStore.searchResults.length).toEqual(0)
    expect(searchStore.currentBook.title).toBeUndefined()
  })

  test('mutations', () => {
    searchStore.$patch({
      searchTerm: 'test',
      numFound: 10
    })

    expect(searchStore.searchTerm).toEqual('test')
    expect(searchStore.numFound).toEqual(10)
  })
})
