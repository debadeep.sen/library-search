/* eslint-disable @typescript-eslint/ban-types */
import { describe, test, expect, beforeEach } from 'vitest'
import { createPinia, setActivePinia, type Store } from 'pinia'
import { useLoaderStore } from '@/stores/loader'

describe('Testing loader store', () => {
  let loader: Store<
    'loader',
    { loading: boolean },
    {},
    { show(): void; hide(): void }
  >
  beforeEach(() => {
    const pinia = createPinia()
    setActivePinia(pinia)
    loader = useLoaderStore()
  })

  test('loading initial value', () => {
    expect(loader.loading).toBe(false)
  })

  test('show', () => {
    loader.show()
    expect(loader.loading).toBe(true)
  })

  test('hide', () => {
    loader.hide()
    expect(loader.loading).toBe(false)
  })
})
