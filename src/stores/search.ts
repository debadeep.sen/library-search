import type { Book } from '@/types/book'
import { defineStore } from 'pinia'

export const useSearchStore = defineStore({
  id: 'search',
  state: () => ({
    searchTerm: '',
    numFound: null as number | null,
    searchResults: [] as Book[],
    currentBook: {} as Book
  }),
  persist: {
    enabled: true,
    strategies: [
      {
        key: 'OLS_SearchStore'
      }
    ]
  }
})
