import { API_BASE_URL, QUERY_TYPES } from './constants'

export const getFullEndPoint = (
  query: string,
  queryType = QUERY_TYPES.GENERAL
): string => `${API_BASE_URL}?${queryType}=${query}`
