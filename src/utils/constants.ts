export const API_BASE_URL = 'https://openlibrary.org/search.json'

export const EVENTS = Object.freeze({
  SEARCH: 'search',
  CLOSE: 'close'
})

export const QUERY_TYPES = Object.freeze({
  GENERAL: 'q',
  TITLE: 'title',
  AUTHOR: 'author'
})

export const UNICODE = Object.freeze({
  BACK: '&#171;',
  CLOSE: '&#10005;'
})
