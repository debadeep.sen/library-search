# library-search

## Live demo

The application has been configured through Gitlab's CI/CD to automatically deploy at http://debadeep.sen.gitlab.io/library-search. Please note that due to the nature of my username (which includes a dot of its own), Gitlab cannot provide an `https` URL for this (because it becomes a nested subdomain), so you might get a security warning when trying to access the page.


## Instructions for running the app locally

Please clone this repository and use the `main` branch.

The app has been built with Vue 3 (scaffolded via Vite), with Pinia as store. It also uses `axios` for making network calls, `vue-router` for page navigation (both static and programmatic) and a collection of packages for unit testing (please see below).

### Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

### Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

### Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

### Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Run unit tests with [Vitest](https://vitest.dev/), [Vue Test Utils](https://test-utils.vuejs.org/), and [`@pinia/testing`](https://www.npmjs.com/package/@pinia/testing)

```sh
npm run test
```
