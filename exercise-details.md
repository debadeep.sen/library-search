# Vue Exercise: 

1. Create a new Vue 3 project using any kind of bootstrapping tools you may desire (Vite, Vue CLI, Nuxt, etc). The project should include the following `npm` packages: `axios`, `lodash`, `Pinia` and `vue-router`.  
2. Add a page that includes a form to enter a book title and a submit button.
3. Upon submitting the form, use axios to get data from the OpenApi library (docs: https://openlibrary.org/dev/docs/api/search) using the query entered in the text box. 
4. Use lodash to retrieve the top 10 titles returned and display in a list on a new page, using vue-router to navigate and Pinia as a data store.  

Be mindful of spacing and code style. Don't worry about making it pretty. 
Upload to a public git repo of your choice (GitHub, GitLab, etc) and send us the link along with any instructions required to run locally. 